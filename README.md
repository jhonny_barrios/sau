# SAU #

El Sistema de Alarmas UNEG (SAU), es una instancia del Sistema de Seguridad UNEG (SSU), permite la lectura de las noticias generadas a través de las alertas enviadas al Centro de Control del Sistema de Seguridad UNEG.


### Características ###

* Listado de noticias.
* Vista en detalle de noticias con punto en el mapa del hecho sucedido y galeria de imagenes.
* Filtro a través de menú lateral por área en el que se desarrollan los hechos.
* Envío de alertas al CCSSU con fotos, descripción y ubicación.

### Screenshots:###

![IMG-20160315-WA0010.jpg](https://bitbucket.org/repo/94BxxE/images/2861001555-IMG-20160315-WA0010.jpg)


![IMG-20160315-WA0009.jpg](https://bitbucket.org/repo/94BxxE/images/2615702554-IMG-20160315-WA0009.jpg)


![IMG-20160307-WA0003.jpg](https://bitbucket.org/repo/94BxxE/images/952208577-IMG-20160307-WA0003.jpg)